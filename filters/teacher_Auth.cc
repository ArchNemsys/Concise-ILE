/*
 * This file is part of the Concise-ILE project.
 * Copyright (C) 2013-2024 Jonathan Kelly jaknemsys@gmail.com
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *    You should have received a copy of the License along with this program.
 *    If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */
/////////////////////////////////////////////////////////////////////

#include "teacher_Auth.h"

using namespace drogon;
using namespace teacher;

void TeacherAuth::doFilter(const HttpRequestPtr &req,
                         FilterCallback &&fcb,
                         FilterChainCallback &&fccb)
{
    //Edit your logic here
    if (req->session()->find("TeacherSession"))
    {
        //Passed
        fccb();
        return;
    }


    //Check failed
    auto res = drogon::HttpResponse::newHttpResponse();
    res->setStatusCode(drogon::k403Forbidden);
    fcb(res);
}
