/*
 * This file is part of the Concise-ILE project.
 * Copyright (C) 2013-2024 Jonathan Kelly jaknemsys@gmail.com
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *    You should have received a copy of the License along with this program.
 *    If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */
/////////////////////////////////////////////////////////////////////

#include "student_Auth.h"

using namespace drogon;
using namespace student;

void StudentAuth::doFilter(const HttpRequestPtr &req,
                         FilterCallback &&fcb,
                         FilterChainCallback &&fccb)
{

    if (req->session()->find("StudentSession"))
    {
        //Passed
        fccb();
        return;
    }


    //Check failed
    auto res = drogon::HttpResponse::newHttpResponse();
    res->setStatusCode(k500InternalServerError);
    fcb(res);
}
