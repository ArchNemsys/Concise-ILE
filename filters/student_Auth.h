/*
 * This file is part of the Concise-ILE project.
 * Copyright (C) 2013-2024 Jonathan Kelly jaknemsys@gmail.com
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *    You should have received a copy of the License along with this program.
 *    If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */
/////////////////////////////////////////////////////////////////////

#ifndef STUDENT_AUTH_H
#define STUDENT_AUTH_H

#include <drogon/HttpFilter.h>
using namespace drogon;
namespace student
{

class StudentAuth : public HttpFilter<StudentAuth>
{
  public:
    StudentAuth() {}
    void doFilter(const HttpRequestPtr &req,
                  FilterCallback &&fcb,
                  FilterChainCallback &&fccb) override;
};

}


#endif
