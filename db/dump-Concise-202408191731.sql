/*!999999\- enable the sandbox mode */ 
-- MariaDB dump 10.19-11.4.2-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: Concise
-- ------------------------------------------------------
-- Server version	11.4.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*M!100616 SET @OLD_NOTE_VERBOSITY=@@NOTE_VERBOSITY, NOTE_VERBOSITY=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `admin_ID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`admin_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assignedquizzes`
--

DROP TABLE IF EXISTS `assignedquizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignedquizzes` (
  `quiz_ID` int(11) NOT NULL,
  `module_ID` int(11) NOT NULL,
  PRIMARY KEY (`quiz_ID`,`module_ID`),
  KEY `AssignedQuizzes_Modules_FK` (`module_ID`),
  CONSTRAINT `AssignedQuizzes_Modules_FK` FOREIGN KEY (`module_ID`) REFERENCES `modules` (`module_ID`),
  CONSTRAINT `AssignedQuizzes_Quizzes_FK` FOREIGN KEY (`quiz_ID`) REFERENCES `quizzes` (`quiz_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assignedquizzes`
--

LOCK TABLES `assignedquizzes` WRITE;
/*!40000 ALTER TABLE `assignedquizzes` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignedquizzes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attempts`
--

DROP TABLE IF EXISTS `attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attempts` (
  `student_ID` int(11) NOT NULL,
  `quiz_ID` int(11) NOT NULL,
  `attempt_no` int(11) NOT NULL,
  `mark_achieved` int(11) DEFAULT NULL,
  `teacher_praise` varchar(100) DEFAULT NULL,
  `teacher_feedback` varchar(100) DEFAULT NULL,
  `student_feedback` varchar(100) DEFAULT NULL,
  `timestamp_submitted` datetime DEFAULT NULL,
  `timestamp_marked` datetime DEFAULT NULL,
  `timestamp_studentfeedback` datetime DEFAULT NULL,
  PRIMARY KEY (`student_ID`,`quiz_ID`,`attempt_no`),
  KEY `Attempts_Tasks_FK` (`quiz_ID`),
  CONSTRAINT `Attempts_Students_FK` FOREIGN KEY (`student_ID`) REFERENCES `students` (`student_ID`),
  CONSTRAINT `Attempts_Tasks_FK` FOREIGN KEY (`quiz_ID`) REFERENCES `quizzes` (`quiz_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attempts`
--

LOCK TABLES `attempts` WRITE;
/*!40000 ALTER TABLE `attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `course_ID` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`course_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enrolment`
--

DROP TABLE IF EXISTS `enrolment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enrolment` (
  `group_ID` int(11) NOT NULL,
  `student_ID` int(11) NOT NULL,
  PRIMARY KEY (`group_ID`,`student_ID`),
  KEY `Enrolment_Students_FK` (`student_ID`),
  CONSTRAINT `Enrolment_Students_FK` FOREIGN KEY (`student_ID`) REFERENCES `students` (`student_ID`),
  CONSTRAINT `Enrolment_TeachingGroups_FK` FOREIGN KEY (`group_ID`) REFERENCES `teachinggroups` (`group_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enrolment`
--

LOCK TABLES `enrolment` WRITE;
/*!40000 ALTER TABLE `enrolment` DISABLE KEYS */;
/*!40000 ALTER TABLE `enrolment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lessons` (
  `lesson_ID` int(11) NOT NULL AUTO_INCREMENT,
  `module_ID` int(11) NOT NULL,
  `group_ID` int(11) NOT NULL,
  `teacher_ID` int(11) NOT NULL,
  `join_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`lesson_ID`),
  KEY `Lessons_Modules_FK` (`module_ID`),
  KEY `Lessons_TeachingGroups_FK` (`group_ID`),
  KEY `Lessons_Teachers_FK` (`teacher_ID`),
  CONSTRAINT `Lessons_Modules_FK` FOREIGN KEY (`module_ID`) REFERENCES `modules` (`module_ID`),
  CONSTRAINT `Lessons_Teachers_FK` FOREIGN KEY (`teacher_ID`) REFERENCES `teachers` (`teacher_ID`),
  CONSTRAINT `Lessons_TeachingGroups_FK` FOREIGN KEY (`group_ID`) REFERENCES `teachinggroups` (`group_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lessons`
--

LOCK TABLES `lessons` WRITE;
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `module_ID` int(11) NOT NULL AUTO_INCREMENT,
  `course_ID` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `parent_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`module_ID`),
  KEY `Modules_Modules_FK` (`parent_ID`),
  KEY `Modules_Courses_FK` (`course_ID`),
  CONSTRAINT `Modules_Courses_FK` FOREIGN KEY (`course_ID`) REFERENCES `courses` (`course_ID`),
  CONSTRAINT `Modules_Modules_FK` FOREIGN KEY (`parent_ID`) REFERENCES `modules` (`module_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `question_ID` int(11) NOT NULL AUTO_INCREMENT,
  `marks` int(11) DEFAULT NULL,
  `question` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`question`)),
  `answer` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`answer`)),
  PRIMARY KEY (`question_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quizquestions`
--

DROP TABLE IF EXISTS `quizquestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quizquestions` (
  `quiz_ID` int(11) NOT NULL,
  `question_ID` int(11) NOT NULL,
  `ordinal` int(11) NOT NULL,
  PRIMARY KEY (`quiz_ID`,`question_ID`),
  KEY `QuizQuestions_Questions_FK` (`question_ID`),
  CONSTRAINT `QuizQuestions_Questions_FK` FOREIGN KEY (`question_ID`) REFERENCES `questions` (`question_ID`),
  CONSTRAINT `QuizQuestions_Quizzes_FK` FOREIGN KEY (`quiz_ID`) REFERENCES `quizzes` (`quiz_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quizquestions`
--

LOCK TABLES `quizquestions` WRITE;
/*!40000 ALTER TABLE `quizquestions` DISABLE KEYS */;
/*!40000 ALTER TABLE `quizquestions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quizzes`
--

DROP TABLE IF EXISTS `quizzes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quizzes` (
  `quiz_ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`quiz_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quizzes`
--

LOCK TABLES `quizzes` WRITE;
/*!40000 ALTER TABLE `quizzes` DISABLE KEYS */;
/*!40000 ALTER TABLE `quizzes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `responses`
--

DROP TABLE IF EXISTS `responses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responses` (
  `student_ID` int(11) NOT NULL,
  `quiz_ID` int(11) NOT NULL,
  `attempt_no` int(11) NOT NULL,
  `question_ID` int(11) NOT NULL,
  `student_attempt` varchar(100) DEFAULT NULL,
  `teacher_feedback` varchar(100) DEFAULT NULL,
  `student_feedback` varchar(100) DEFAULT NULL,
  `mark` int(11) DEFAULT NULL,
  PRIMARY KEY (`student_ID`,`quiz_ID`,`attempt_no`,`question_ID`),
  KEY `Responses_QuizQuestions_FK` (`quiz_ID`,`question_ID`),
  CONSTRAINT `Responses_QuizQuestions_FK` FOREIGN KEY (`quiz_ID`, `question_ID`) REFERENCES `quizquestions` (`quiz_ID`, `question_ID`),
  CONSTRAINT `Responses_Students_FK` FOREIGN KEY (`student_ID`) REFERENCES `students` (`student_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responses`
--

LOCK TABLES `responses` WRITE;
/*!40000 ALTER TABLE `responses` DISABLE KEYS */;
/*!40000 ALTER TABLE `responses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `student_ID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `UPN` varchar(100) DEFAULT NULL,
  `UUID` uuid NOT NULL,
  PRIMARY KEY (`student_ID`),
  UNIQUE KEY `Students_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `teacher_ID` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `UUID` uuid NOT NULL,
  PRIMARY KEY (`teacher_ID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES
(1,'Mr Teacher','Teacher1@org.uk','802fd8b3-2c9c-4026-89e9-8f8d60fb00ab');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachersgroups`
--

DROP TABLE IF EXISTS `teachersgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachersgroups` (
  `teacher_ID` int(11) NOT NULL,
  `group_ID` int(11) NOT NULL,
  PRIMARY KEY (`teacher_ID`,`group_ID`),
  KEY `TeachersGroups_TeachingGroups_FK` (`group_ID`),
  CONSTRAINT `TeachersGroups_Teachers_FK` FOREIGN KEY (`teacher_ID`) REFERENCES `teachers` (`teacher_ID`),
  CONSTRAINT `TeachersGroups_TeachingGroups_FK` FOREIGN KEY (`group_ID`) REFERENCES `teachinggroups` (`group_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachersgroups`
--

LOCK TABLES `teachersgroups` WRITE;
/*!40000 ALTER TABLE `teachersgroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `teachersgroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachinggroups`
--

DROP TABLE IF EXISTS `teachinggroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachinggroups` (
  `group_ID` int(11) NOT NULL AUTO_INCREMENT,
  `group_code` varchar(100) NOT NULL,
  `course_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_ID`),
  UNIQUE KEY `TeachingGroups_UNIQUE` (`group_code`),
  KEY `TeachingGroups_Courses_FK` (`course_ID`),
  CONSTRAINT `TeachingGroups_Courses_FK` FOREIGN KEY (`course_ID`) REFERENCES `courses` (`course_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachinggroups`
--

LOCK TABLES `teachinggroups` WRITE;
/*!40000 ALTER TABLE `teachinggroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `teachinggroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'Concise'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*M!100616 SET NOTE_VERBOSITY=@OLD_NOTE_VERBOSITY */;

-- Dump completed on 2024-08-19 17:31:03
