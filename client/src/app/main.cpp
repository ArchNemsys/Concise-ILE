#include "passworddialog.h"

#include <QApplication>
#include <QDebug>

#include "mainwindow.h"
#include "session.h"
#include "ile.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Session session;
    auto args = QCoreApplication::arguments();
    if ( args.size() > 1  &&  args.at(1) == "teacher" )
    {
        // running from teacher shortcut
        PasswordDialog passwordDialog;

        // not working ?
        if (passwordDialog.exec() == QDialog::Accepted)
        {
            session.setRole("teacher");

        }
        else
        {
            return -1;
            //QMetaObject::invokeMethod(qApp, "quit", Qt::QueuedConnection);
        }

    }


    session.loadConfig("config.json");
    ILE app(session);

    MainWindow w;
    w.show();
    return a.exec();

}
