#include "session.h"

#include <QUrlQuery>
#include <QRestReply>
#include <QJsonDocument>

#include<QDebug>

Session::Session(QObject *parent) : QObject{parent},
    m_rest_access_manager(std::make_unique<QRestAccessManager>(&net_manager)),
    m_factory(std::make_unique<QNetworkRequestFactory>())
{
    m_factory->setBaseUrl(QUrl("http://127.0.0.1:8080"));
}

bool Session::loginSession(QString role, QString email, QString uuid)
{
    bool hasSession = false;

    QUrlQuery query;
    query.setQueryItems({   {"role", role},
                            {"email", email},
                            {"uuid", uuid}      });

    qDebug() << query.toString(QUrl::FullyEncoded).toUtf8();

    m_rest_access_manager->post(m_factory->createRequest("/login"), query.toString(QUrl::FullyEncoded).toUtf8(), this, [this](QRestReply &reply) {

        if(reply.isSuccess()){
            //Turn the data into a json document
            auto doc = reply.readJson();

            qDebug() << doc;
        }
        else
        {
            qDebug() << reply.errorString();
        }

    });

    return hasSession;
}
