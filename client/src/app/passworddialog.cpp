#include "passworddialog.h"
#include "ui_passworddialog.h"

PasswordDialog::PasswordDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::PasswordDialog)
{
    ui->setupUi(this);
}

PasswordDialog::~PasswordDialog()
{
    delete ui;
}




void PasswordDialog::on_Confirmbtn_clicked()
{
    ui->passwordLineEdit->text();
    if(ui->passwordLineEdit->text() == "StaffOnly")
    {
        this->done(QDialog::Accepted);
    }

    this->done(QDialog::Rejected);
}

