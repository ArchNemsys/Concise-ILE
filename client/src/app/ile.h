#ifndef ILE_H
#define ILE_H

#include <QObject>

//forwards
class Session;

class ILE : public QObject
{
    Q_OBJECT
public:
    explicit ILE(const Session& session, QObject *parent = nullptr);

signals:

private:
    const Session& m_session;
};

#endif // ILE_H
