#ifndef SESSION_H
#define SESSION_H

#include <QObject>

class Session : public QObject
{
    Q_OBJECT
public:
    explicit Session(QString role="student",QObject *parent = nullptr);

    void setRole(QString role){m_role = role;};
    bool loadConfig(QString path);
signals:

private:
    QString m_role;
    QString m_hostAddress;
};

#endif // SESSION_H
