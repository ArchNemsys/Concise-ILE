#include "session.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>


Session::Session(QString role,QObject *parent)
    : m_role{role}
    , QObject{parent}
{}



bool Session::loadConfig(QString path)
{

        QFile file( path );
        if( file.open( QIODevice::ReadOnly ) )
        {
            QByteArray bytes = file.readAll();
            file.close();

            QJsonParseError jsonError;
            QJsonDocument document = QJsonDocument::fromJson( bytes, &jsonError );

            if( jsonError.error != QJsonParseError::NoError )
            {
                qDebug() << "fromJson failed: " << jsonError.errorString().toStdString();
                return false;
            }

            if( !document.isObject() )
            {
                qDebug()<<"JSON is not an object.";
                return false;
            }

            QJsonObject jsonObj = document.object();

            if(jsonObj.isEmpty())
            {
                qDebug()<<"JSON object is empty.";
                return false;
            }

            m_hostAddress = jsonObj.value("hostAddress").toString();

            qDebug() << m_role;
            qDebug() << m_hostAddress;
            return true;
        }

return false;
}
