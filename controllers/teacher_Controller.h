/*
 * This file is part of the Concise-ILE project.
 * Copyright (C) 2013-2024 Jonathan Kelly jaknemsys@gmail.com
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *    You should have received a copy of the License along with this program.
 *    If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */
/////////////////////////////////////////////////////////////////////

#ifndef TEACHER_CONTROLLER_H
#define TEACHER_CONTROLLER_H

#include <drogon/HttpController.h>

using namespace drogon;

namespace teacher
{
class TeacherController : public drogon::HttpController<TeacherController>
{
  public:
    METHOD_LIST_BEGIN
    //    ADD_METHOD_TO(TeacherController::login,"/teacherlogin?email={1}&uuid={2}",Post); //public

    // METHOD_ADD(Controller::get, "/{2}/{1}", Get); // path is /teacher/Controller/{arg2}/{arg1}
    // METHOD_ADD(Controller::your_method_name, "/{1}/{2}/list", Get); // path is /teacher/Controller/{arg1}/{arg2}/list
    // ADD_METHOD_TO(Controller::your_method_name, "/absolute/path/{1}/{2}/list", Get); // path is /absolute/path/{arg1}/{arg2}/list

    METHOD_LIST_END
    // your declaration of processing function maybe like this:
    // void get(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback, int p1, std::string p2);
    // void your_method_name(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback, double p1, int p2) const;


};
}

#endif
