/**
 *
 *  Quizquestions.cc
 *  DO NOT EDIT. This file is generated by drogon_ctl
 *
 */

#include "Quizquestions.h"
#include "Questions.h"
#include "Quizzes.h"
#include <drogon/utils/Utilities.h>
#include <string>

using namespace drogon;
using namespace drogon::orm;
using namespace drogon_model::Concise;

const std::string Quizquestions::Cols::_quiz_ID = "quiz_ID";
const std::string Quizquestions::Cols::_question_ID = "question_ID";
const std::string Quizquestions::Cols::_ordinal = "ordinal";
const std::vector<std::string> Quizquestions::primaryKeyName = {"quiz_ID","question_ID"};
const bool Quizquestions::hasPrimaryKey = true;
const std::string Quizquestions::tableName = "quizquestions";

const std::vector<typename Quizquestions::MetaData> Quizquestions::metaData_={
{"quiz_ID","int32_t","int(11)",4,0,1,1},
{"question_ID","int32_t","int(11)",4,0,1,1},
{"ordinal","int32_t","int(11)",4,0,0,1}
};
const std::string &Quizquestions::getColumnName(size_t index) noexcept(false)
{
    assert(index < metaData_.size());
    return metaData_[index].colName_;
}
Quizquestions::Quizquestions(const Row &r, const ssize_t indexOffset) noexcept
{
    if(indexOffset < 0)
    {
        if(!r["quiz_ID"].isNull())
        {
            quizId_=std::make_shared<int32_t>(r["quiz_ID"].as<int32_t>());
        }
        if(!r["question_ID"].isNull())
        {
            questionId_=std::make_shared<int32_t>(r["question_ID"].as<int32_t>());
        }
        if(!r["ordinal"].isNull())
        {
            ordinal_=std::make_shared<int32_t>(r["ordinal"].as<int32_t>());
        }
    }
    else
    {
        size_t offset = (size_t)indexOffset;
        if(offset + 3 > r.size())
        {
            LOG_FATAL << "Invalid SQL result for this model";
            return;
        }
        size_t index;
        index = offset + 0;
        if(!r[index].isNull())
        {
            quizId_=std::make_shared<int32_t>(r[index].as<int32_t>());
        }
        index = offset + 1;
        if(!r[index].isNull())
        {
            questionId_=std::make_shared<int32_t>(r[index].as<int32_t>());
        }
        index = offset + 2;
        if(!r[index].isNull())
        {
            ordinal_=std::make_shared<int32_t>(r[index].as<int32_t>());
        }
    }

}

Quizquestions::Quizquestions(const Json::Value &pJson, const std::vector<std::string> &pMasqueradingVector) noexcept(false)
{
    if(pMasqueradingVector.size() != 3)
    {
        LOG_ERROR << "Bad masquerading vector";
        return;
    }
    if(!pMasqueradingVector[0].empty() && pJson.isMember(pMasqueradingVector[0]))
    {
        dirtyFlag_[0] = true;
        if(!pJson[pMasqueradingVector[0]].isNull())
        {
            quizId_=std::make_shared<int32_t>((int32_t)pJson[pMasqueradingVector[0]].asInt64());
        }
    }
    if(!pMasqueradingVector[1].empty() && pJson.isMember(pMasqueradingVector[1]))
    {
        dirtyFlag_[1] = true;
        if(!pJson[pMasqueradingVector[1]].isNull())
        {
            questionId_=std::make_shared<int32_t>((int32_t)pJson[pMasqueradingVector[1]].asInt64());
        }
    }
    if(!pMasqueradingVector[2].empty() && pJson.isMember(pMasqueradingVector[2]))
    {
        dirtyFlag_[2] = true;
        if(!pJson[pMasqueradingVector[2]].isNull())
        {
            ordinal_=std::make_shared<int32_t>((int32_t)pJson[pMasqueradingVector[2]].asInt64());
        }
    }
}

Quizquestions::Quizquestions(const Json::Value &pJson) noexcept(false)
{
    if(pJson.isMember("quiz_ID"))
    {
        dirtyFlag_[0]=true;
        if(!pJson["quiz_ID"].isNull())
        {
            quizId_=std::make_shared<int32_t>((int32_t)pJson["quiz_ID"].asInt64());
        }
    }
    if(pJson.isMember("question_ID"))
    {
        dirtyFlag_[1]=true;
        if(!pJson["question_ID"].isNull())
        {
            questionId_=std::make_shared<int32_t>((int32_t)pJson["question_ID"].asInt64());
        }
    }
    if(pJson.isMember("ordinal"))
    {
        dirtyFlag_[2]=true;
        if(!pJson["ordinal"].isNull())
        {
            ordinal_=std::make_shared<int32_t>((int32_t)pJson["ordinal"].asInt64());
        }
    }
}

void Quizquestions::updateByMasqueradedJson(const Json::Value &pJson,
                                            const std::vector<std::string> &pMasqueradingVector) noexcept(false)
{
    if(pMasqueradingVector.size() != 3)
    {
        LOG_ERROR << "Bad masquerading vector";
        return;
    }
    if(!pMasqueradingVector[0].empty() && pJson.isMember(pMasqueradingVector[0]))
    {
        if(!pJson[pMasqueradingVector[0]].isNull())
        {
            quizId_=std::make_shared<int32_t>((int32_t)pJson[pMasqueradingVector[0]].asInt64());
        }
    }
    if(!pMasqueradingVector[1].empty() && pJson.isMember(pMasqueradingVector[1]))
    {
        if(!pJson[pMasqueradingVector[1]].isNull())
        {
            questionId_=std::make_shared<int32_t>((int32_t)pJson[pMasqueradingVector[1]].asInt64());
        }
    }
    if(!pMasqueradingVector[2].empty() && pJson.isMember(pMasqueradingVector[2]))
    {
        dirtyFlag_[2] = true;
        if(!pJson[pMasqueradingVector[2]].isNull())
        {
            ordinal_=std::make_shared<int32_t>((int32_t)pJson[pMasqueradingVector[2]].asInt64());
        }
    }
}

void Quizquestions::updateByJson(const Json::Value &pJson) noexcept(false)
{
    if(pJson.isMember("quiz_ID"))
    {
        if(!pJson["quiz_ID"].isNull())
        {
            quizId_=std::make_shared<int32_t>((int32_t)pJson["quiz_ID"].asInt64());
        }
    }
    if(pJson.isMember("question_ID"))
    {
        if(!pJson["question_ID"].isNull())
        {
            questionId_=std::make_shared<int32_t>((int32_t)pJson["question_ID"].asInt64());
        }
    }
    if(pJson.isMember("ordinal"))
    {
        dirtyFlag_[2] = true;
        if(!pJson["ordinal"].isNull())
        {
            ordinal_=std::make_shared<int32_t>((int32_t)pJson["ordinal"].asInt64());
        }
    }
}

const int32_t &Quizquestions::getValueOfQuizId() const noexcept
{
    static const int32_t defaultValue = int32_t();
    if(quizId_)
        return *quizId_;
    return defaultValue;
}
const std::shared_ptr<int32_t> &Quizquestions::getQuizId() const noexcept
{
    return quizId_;
}
void Quizquestions::setQuizId(const int32_t &pQuizId) noexcept
{
    quizId_ = std::make_shared<int32_t>(pQuizId);
    dirtyFlag_[0] = true;
}

const int32_t &Quizquestions::getValueOfQuestionId() const noexcept
{
    static const int32_t defaultValue = int32_t();
    if(questionId_)
        return *questionId_;
    return defaultValue;
}
const std::shared_ptr<int32_t> &Quizquestions::getQuestionId() const noexcept
{
    return questionId_;
}
void Quizquestions::setQuestionId(const int32_t &pQuestionId) noexcept
{
    questionId_ = std::make_shared<int32_t>(pQuestionId);
    dirtyFlag_[1] = true;
}

const int32_t &Quizquestions::getValueOfOrdinal() const noexcept
{
    static const int32_t defaultValue = int32_t();
    if(ordinal_)
        return *ordinal_;
    return defaultValue;
}
const std::shared_ptr<int32_t> &Quizquestions::getOrdinal() const noexcept
{
    return ordinal_;
}
void Quizquestions::setOrdinal(const int32_t &pOrdinal) noexcept
{
    ordinal_ = std::make_shared<int32_t>(pOrdinal);
    dirtyFlag_[2] = true;
}

void Quizquestions::updateId(const uint64_t id)
{
}
typename Quizquestions::PrimaryKeyType Quizquestions::getPrimaryKey() const
{
    return std::make_tuple(*quizId_,*questionId_);
}

const std::vector<std::string> &Quizquestions::insertColumns() noexcept
{
    static const std::vector<std::string> inCols={
        "quiz_ID",
        "question_ID",
        "ordinal"
    };
    return inCols;
}

void Quizquestions::outputArgs(drogon::orm::internal::SqlBinder &binder) const
{
    if(dirtyFlag_[0])
    {
        if(getQuizId())
        {
            binder << getValueOfQuizId();
        }
        else
        {
            binder << nullptr;
        }
    }
    if(dirtyFlag_[1])
    {
        if(getQuestionId())
        {
            binder << getValueOfQuestionId();
        }
        else
        {
            binder << nullptr;
        }
    }
    if(dirtyFlag_[2])
    {
        if(getOrdinal())
        {
            binder << getValueOfOrdinal();
        }
        else
        {
            binder << nullptr;
        }
    }
}

const std::vector<std::string> Quizquestions::updateColumns() const
{
    std::vector<std::string> ret;
    if(dirtyFlag_[0])
    {
        ret.push_back(getColumnName(0));
    }
    if(dirtyFlag_[1])
    {
        ret.push_back(getColumnName(1));
    }
    if(dirtyFlag_[2])
    {
        ret.push_back(getColumnName(2));
    }
    return ret;
}

void Quizquestions::updateArgs(drogon::orm::internal::SqlBinder &binder) const
{
    if(dirtyFlag_[0])
    {
        if(getQuizId())
        {
            binder << getValueOfQuizId();
        }
        else
        {
            binder << nullptr;
        }
    }
    if(dirtyFlag_[1])
    {
        if(getQuestionId())
        {
            binder << getValueOfQuestionId();
        }
        else
        {
            binder << nullptr;
        }
    }
    if(dirtyFlag_[2])
    {
        if(getOrdinal())
        {
            binder << getValueOfOrdinal();
        }
        else
        {
            binder << nullptr;
        }
    }
}
Json::Value Quizquestions::toJson() const
{
    Json::Value ret;
    if(getQuizId())
    {
        ret["quiz_ID"]=getValueOfQuizId();
    }
    else
    {
        ret["quiz_ID"]=Json::Value();
    }
    if(getQuestionId())
    {
        ret["question_ID"]=getValueOfQuestionId();
    }
    else
    {
        ret["question_ID"]=Json::Value();
    }
    if(getOrdinal())
    {
        ret["ordinal"]=getValueOfOrdinal();
    }
    else
    {
        ret["ordinal"]=Json::Value();
    }
    return ret;
}

Json::Value Quizquestions::toMasqueradedJson(
    const std::vector<std::string> &pMasqueradingVector) const
{
    Json::Value ret;
    if(pMasqueradingVector.size() == 3)
    {
        if(!pMasqueradingVector[0].empty())
        {
            if(getQuizId())
            {
                ret[pMasqueradingVector[0]]=getValueOfQuizId();
            }
            else
            {
                ret[pMasqueradingVector[0]]=Json::Value();
            }
        }
        if(!pMasqueradingVector[1].empty())
        {
            if(getQuestionId())
            {
                ret[pMasqueradingVector[1]]=getValueOfQuestionId();
            }
            else
            {
                ret[pMasqueradingVector[1]]=Json::Value();
            }
        }
        if(!pMasqueradingVector[2].empty())
        {
            if(getOrdinal())
            {
                ret[pMasqueradingVector[2]]=getValueOfOrdinal();
            }
            else
            {
                ret[pMasqueradingVector[2]]=Json::Value();
            }
        }
        return ret;
    }
    LOG_ERROR << "Masquerade failed";
    if(getQuizId())
    {
        ret["quiz_ID"]=getValueOfQuizId();
    }
    else
    {
        ret["quiz_ID"]=Json::Value();
    }
    if(getQuestionId())
    {
        ret["question_ID"]=getValueOfQuestionId();
    }
    else
    {
        ret["question_ID"]=Json::Value();
    }
    if(getOrdinal())
    {
        ret["ordinal"]=getValueOfOrdinal();
    }
    else
    {
        ret["ordinal"]=Json::Value();
    }
    return ret;
}

bool Quizquestions::validateJsonForCreation(const Json::Value &pJson, std::string &err)
{
    if(pJson.isMember("quiz_ID"))
    {
        if(!validJsonOfField(0, "quiz_ID", pJson["quiz_ID"], err, true))
            return false;
    }
    else
    {
        err="The quiz_ID column cannot be null";
        return false;
    }
    if(pJson.isMember("question_ID"))
    {
        if(!validJsonOfField(1, "question_ID", pJson["question_ID"], err, true))
            return false;
    }
    else
    {
        err="The question_ID column cannot be null";
        return false;
    }
    if(pJson.isMember("ordinal"))
    {
        if(!validJsonOfField(2, "ordinal", pJson["ordinal"], err, true))
            return false;
    }
    else
    {
        err="The ordinal column cannot be null";
        return false;
    }
    return true;
}
bool Quizquestions::validateMasqueradedJsonForCreation(const Json::Value &pJson,
                                                       const std::vector<std::string> &pMasqueradingVector,
                                                       std::string &err)
{
    if(pMasqueradingVector.size() != 3)
    {
        err = "Bad masquerading vector";
        return false;
    }
    try {
      if(!pMasqueradingVector[0].empty())
      {
          if(pJson.isMember(pMasqueradingVector[0]))
          {
              if(!validJsonOfField(0, pMasqueradingVector[0], pJson[pMasqueradingVector[0]], err, true))
                  return false;
          }
        else
        {
            err="The " + pMasqueradingVector[0] + " column cannot be null";
            return false;
        }
      }
      if(!pMasqueradingVector[1].empty())
      {
          if(pJson.isMember(pMasqueradingVector[1]))
          {
              if(!validJsonOfField(1, pMasqueradingVector[1], pJson[pMasqueradingVector[1]], err, true))
                  return false;
          }
        else
        {
            err="The " + pMasqueradingVector[1] + " column cannot be null";
            return false;
        }
      }
      if(!pMasqueradingVector[2].empty())
      {
          if(pJson.isMember(pMasqueradingVector[2]))
          {
              if(!validJsonOfField(2, pMasqueradingVector[2], pJson[pMasqueradingVector[2]], err, true))
                  return false;
          }
        else
        {
            err="The " + pMasqueradingVector[2] + " column cannot be null";
            return false;
        }
      }
    }
    catch(const Json::LogicError &e)
    {
      err = e.what();
      return false;
    }
    return true;
}
bool Quizquestions::validateJsonForUpdate(const Json::Value &pJson, std::string &err)
{
    if(pJson.isMember("quiz_ID"))
    {
        if(!validJsonOfField(0, "quiz_ID", pJson["quiz_ID"], err, false))
            return false;
    }
    else
    {
        err = "The value of primary key must be set in the json object for update";
        return false;
    }
    if(pJson.isMember("question_ID"))
    {
        if(!validJsonOfField(1, "question_ID", pJson["question_ID"], err, false))
            return false;
    }
    else
    {
        err = "The value of primary key must be set in the json object for update";
        return false;
    }
    if(pJson.isMember("ordinal"))
    {
        if(!validJsonOfField(2, "ordinal", pJson["ordinal"], err, false))
            return false;
    }
    return true;
}
bool Quizquestions::validateMasqueradedJsonForUpdate(const Json::Value &pJson,
                                                     const std::vector<std::string> &pMasqueradingVector,
                                                     std::string &err)
{
    if(pMasqueradingVector.size() != 3)
    {
        err = "Bad masquerading vector";
        return false;
    }
    try {
      if(!pMasqueradingVector[0].empty() && pJson.isMember(pMasqueradingVector[0]))
      {
          if(!validJsonOfField(0, pMasqueradingVector[0], pJson[pMasqueradingVector[0]], err, false))
              return false;
      }
    else
    {
        err = "The value of primary key must be set in the json object for update";
        return false;
    }
      if(!pMasqueradingVector[1].empty() && pJson.isMember(pMasqueradingVector[1]))
      {
          if(!validJsonOfField(1, pMasqueradingVector[1], pJson[pMasqueradingVector[1]], err, false))
              return false;
      }
    else
    {
        err = "The value of primary key must be set in the json object for update";
        return false;
    }
      if(!pMasqueradingVector[2].empty() && pJson.isMember(pMasqueradingVector[2]))
      {
          if(!validJsonOfField(2, pMasqueradingVector[2], pJson[pMasqueradingVector[2]], err, false))
              return false;
      }
    }
    catch(const Json::LogicError &e)
    {
      err = e.what();
      return false;
    }
    return true;
}
bool Quizquestions::validJsonOfField(size_t index,
                                     const std::string &fieldName,
                                     const Json::Value &pJson,
                                     std::string &err,
                                     bool isForCreation)
{
    switch(index)
    {
        case 0:
            if(pJson.isNull())
            {
                err="The " + fieldName + " column cannot be null";
                return false;
            }
            if(!pJson.isInt())
            {
                err="Type error in the "+fieldName+" field";
                return false;
            }
            break;
        case 1:
            if(pJson.isNull())
            {
                err="The " + fieldName + " column cannot be null";
                return false;
            }
            if(!pJson.isInt())
            {
                err="Type error in the "+fieldName+" field";
                return false;
            }
            break;
        case 2:
            if(pJson.isNull())
            {
                err="The " + fieldName + " column cannot be null";
                return false;
            }
            if(!pJson.isInt())
            {
                err="Type error in the "+fieldName+" field";
                return false;
            }
            break;
        default:
            err="Internal error in the server";
            return false;
    }
    return true;
}
Questions Quizquestions::getQuestions(const DbClientPtr &clientPtr) const {
    static const std::string sql = "select * from questions where question_id = ?";
    Result r(nullptr);
    {
        auto binder = *clientPtr << sql;
        binder << *questionId_ << Mode::Blocking >>
            [&r](const Result &result) { r = result; };
        binder.exec();
    }
    if (r.size() == 0)
    {
        throw UnexpectedRows("0 rows found");
    }
    else if (r.size() > 1)
    {
        throw UnexpectedRows("Found more than one row");
    }
    return Questions(r[0]);
}

void Quizquestions::getQuestions(const DbClientPtr &clientPtr,
                                 const std::function<void(Questions)> &rcb,
                                 const ExceptionCallback &ecb) const
{
    static const std::string sql = "select * from questions where question_id = ?";
    *clientPtr << sql
               << *questionId_
               >> [rcb = std::move(rcb), ecb](const Result &r){
                    if (r.size() == 0)
                    {
                        ecb(UnexpectedRows("0 rows found"));
                    }
                    else if (r.size() > 1)
                    {
                        ecb(UnexpectedRows("Found more than one row"));
                    }
                    else
                    {
                        rcb(Questions(r[0]));
                    }
               }
               >> ecb;
}
Quizzes Quizquestions::getQuizzes(const DbClientPtr &clientPtr) const {
    static const std::string sql = "select * from quizzes where quiz_id = ?";
    Result r(nullptr);
    {
        auto binder = *clientPtr << sql;
        binder << *quizId_ << Mode::Blocking >>
            [&r](const Result &result) { r = result; };
        binder.exec();
    }
    if (r.size() == 0)
    {
        throw UnexpectedRows("0 rows found");
    }
    else if (r.size() > 1)
    {
        throw UnexpectedRows("Found more than one row");
    }
    return Quizzes(r[0]);
}

void Quizquestions::getQuizzes(const DbClientPtr &clientPtr,
                               const std::function<void(Quizzes)> &rcb,
                               const ExceptionCallback &ecb) const
{
    static const std::string sql = "select * from quizzes where quiz_id = ?";
    *clientPtr << sql
               << *quizId_
               >> [rcb = std::move(rcb), ecb](const Result &r){
                    if (r.size() == 0)
                    {
                        ecb(UnexpectedRows("0 rows found"));
                    }
                    else if (r.size() > 1)
                    {
                        ecb(UnexpectedRows("Found more than one row"));
                    }
                    else
                    {
                        rcb(Quizzes(r[0]));
                    }
               }
               >> ecb;
}
