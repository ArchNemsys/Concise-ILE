/**
 *
 *  RestfulResponsesCtrl.cc
 *  This file is generated by drogon_ctl
 *
 */

#include "RestfulResponsesCtrl.h"
#include <string>



void RestfulResponsesCtrl::get(const HttpRequestPtr &req,
                               std::function<void(const HttpResponsePtr &)> &&callback)
{
    RestfulResponsesCtrlBase::get(req, std::move(callback));
}

void RestfulResponsesCtrl::create(const HttpRequestPtr &req,
                                  std::function<void(const HttpResponsePtr &)> &&callback)
{
    RestfulResponsesCtrlBase::create(req, std::move(callback));
}
