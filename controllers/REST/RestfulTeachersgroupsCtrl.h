/**
 *
 *  RestfulTeachersgroupsCtrl.h
 *  This file is generated by drogon_ctl
 *
 */

#pragma once

#include <drogon/HttpController.h>
#include "RestfulTeachersgroupsCtrlBase.h"

#include "Teachersgroups.h"
using namespace drogon;
using namespace drogon_model::Concise;
/**
 * @brief this class is created by the drogon_ctl command.
 * this class is a restful API controller for reading and writing the teachersgroups table.
 */

class RestfulTeachersgroupsCtrl: public drogon::HttpController<RestfulTeachersgroupsCtrl>, public RestfulTeachersgroupsCtrlBase
{
  public:
    METHOD_LIST_BEGIN
    ADD_METHOD_TO(RestfulTeachersgroupsCtrl::get,"/rest/teachersgroups",Get,Options);
    ADD_METHOD_TO(RestfulTeachersgroupsCtrl::create,"/rest/teachersgroups",Post,Options);
    //ADD_METHOD_TO(RestfulTeachersgroupsCtrl::update,"/rest/teachersgroups",Put,Options);
    METHOD_LIST_END
     
    void get(const HttpRequestPtr &req,
             std::function<void(const HttpResponsePtr &)> &&callback);
    void create(const HttpRequestPtr &req,
             std::function<void(const HttpResponsePtr &)> &&callback);

};
