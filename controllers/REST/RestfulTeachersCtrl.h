/**
 *
 *  RestfulTeachersCtrl.h
 *  This file is generated by drogon_ctl
 *
 */

#pragma once

#include <drogon/HttpController.h>
#include "RestfulTeachersCtrlBase.h"

#include "Teachers.h"
using namespace drogon;
using namespace drogon_model::Concise;
/**
 * @brief this class is created by the drogon_ctl command.
 * this class is a restful API controller for reading and writing the teachers table.
 */

class RestfulTeachersCtrl: public drogon::HttpController<RestfulTeachersCtrl>, public RestfulTeachersCtrlBase
{
  public:
    METHOD_LIST_BEGIN
    ADD_METHOD_TO(RestfulTeachersCtrl::getOne,"/rest/teachers/{1}",Get,Options);
    ADD_METHOD_TO(RestfulTeachersCtrl::updateOne,"/rest/teachers/{1}",Put,Options);
    ADD_METHOD_TO(RestfulTeachersCtrl::deleteOne,"/rest/teachers/{1}",Delete,Options);
    ADD_METHOD_TO(RestfulTeachersCtrl::get,"/rest/teachers",Get,Options);
    ADD_METHOD_TO(RestfulTeachersCtrl::create,"/rest/teachers",Post,Options);
    //ADD_METHOD_TO(RestfulTeachersCtrl::update,"/rest/teachers",Put,Options);
    METHOD_LIST_END
     
    void getOne(const HttpRequestPtr &req,
                std::function<void(const HttpResponsePtr &)> &&callback,
                Teachers::PrimaryKeyType &&id);
    void updateOne(const HttpRequestPtr &req,
                   std::function<void(const HttpResponsePtr &)> &&callback,
                   Teachers::PrimaryKeyType &&id);
    void deleteOne(const HttpRequestPtr &req,
                   std::function<void(const HttpResponsePtr &)> &&callback,
                   Teachers::PrimaryKeyType &&id);
    void get(const HttpRequestPtr &req,
             std::function<void(const HttpResponsePtr &)> &&callback);
    void create(const HttpRequestPtr &req,
             std::function<void(const HttpResponsePtr &)> &&callback);

};
