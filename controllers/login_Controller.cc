/*
 * This file is part of the Concise-ILE project.
 * Copyright (C) 2013-2024 Jonathan Kelly jaknemsys@gmail.com
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *    You should have received a copy of the License along with this program.
 *    If not, see <https://www.gnu.org/licenses/>.
 *
 *
 */
/////////////////////////////////////////////////////////////////////

#include "login_Controller.h"

#include <drogon/drogon.h>
#include <drogon/orm/CoroMapper.h>
#include <drogon/orm/DbClient.h>
#include <drogon/orm/DbTypes.h>
//#include <string_view>
//#include <drogon/orm/QueryBuilder.h>

#include "Teachinggroups.h"
#include "Lessons.h"
#include "Enrolment.h"

using namespace drogon::orm;
using namespace drogon_model::Concise;

void dbexception(const drogon::orm::DrogonDbException &e)
{
    std::cerr << "error:" << e.base().what() << std::endl;
}


void LoginController::login(const HttpRequestPtr &req, std::function<void (const HttpResponsePtr &)> &&callback)
{
    auto params = req->getParameters();
    auto clientPtr = drogon::app().getDbClient();

    if(params.contains("role") && params.contains("email") && params.contains("uuid"))
    {
        std::string role = std::move(params.at("role"));
        std::string email = std::move(params.at("email"));
        std::string uuid = std::move(params.at("uuid"));

        LOG_DEBUG<<role<<" login attempt "<<email;

        if(role == "teacher")
        {
            clientPtr->execSqlAsync("SELECT * FROM teachers WHERE teachers.email = ?;",
                                    [req,uuid,callback](const drogon::orm::Result &result)
                                    {

                                        if(!result.empty())
                                        {
                                            auto row = result.front();

                                            if(row["UUID"].as<std::string>() == uuid)
                                            {
                                                req->session()->insert("teacher_ID", row["teacher_ID"].as<int>() );
                                                req->session()->insert("teacher_name", row["teacher_name"].as<std::string>() );

                                                Json::Value ret;
                                                ret["teacher_name"]=req->session()->get<std::string>("teacher_name");
                                                //ret["token"]=drogon::utils::getUuid();
                                                auto resp=HttpResponse::newHttpJsonResponse(ret);

                                                callback(resp);
                                            }
                                            else
                                            {
                                                //Auth failed
                                                auto resp = drogon::HttpResponse::newHttpResponse();
                                                resp->setStatusCode(drogon::k401Unauthorized);
                                                callback(resp);
                                            }
                                        }
                                    },
                                    dbexception,
                                    email);
        }
        else if(role == "student")
        {
            clientPtr->execSqlAsync("SELECT * FROM students WHERE students.email = ?;",
                                    [req,uuid,callback](const drogon::orm::Result &result)
                                    {
                                        if(!result.empty())
                                        {
                                            auto row = result.front();

                                            if(row["UUID"].as<std::string>() == uuid)
                                            {
                                                req->session()->insert("student_ID", row["student_ID"].as<int>() );
                                                req->session()->insert("student_name", (row["firstname"].as<std::string>() +" " + row["lastname"].as<std::string>()) );

                                                Json::Value ret;
                                                ret["student_name"]=req->session()->get<std::string>("student_name");
                                                //ret["token"]=drogon::utils::getUuid();
                                                auto resp=HttpResponse::newHttpJsonResponse(ret);
                                                callback(resp);
                                            }
                                            else
                                            {
                                                //Auth failed
                                                auto resp = drogon::HttpResponse::newHttpResponse();
                                                resp->setStatusCode(drogon::k401Unauthorized);
                                                callback(resp);
                                            }
                                        }
                                    },
                                    dbexception,
                                    email);
        }

    }
    else
    {
        LOG_DEBUG<<"Malformed login attempt";
    }


}



void LoginController::enroll(const HttpRequestPtr &req, std::function<void (const HttpResponsePtr &)> &&callback)
{
    auto params = req->getParameters();
    auto session = req->session();

    if(params.contains("student_email") && params.contains("group_code") && params.contains("join_code"))
    {
        auto clientPtr = drogon::app().getDbClient();

        //params.at("group_code");

            clientPtr->execSqlAsync("SELECT * FROM lessons INNER JOIN teachinggroups ON lessons.group_ID = teachinggroups.group_ID WHERE group_code = ?;",
                                    [clientPtr,params,session,callback](const drogon::orm::Result &result)
                                    {
                                        if(result.size() == 1)
                                        {
                                            auto row = result.front();
                                            if(row["join_code"].as<std::string>() == params.at("join_code"))
                                            {
                                                // possily use same transaction, if race condition
                                                int group_id = row["group_id"].as<int>();

                                                if(session->find("student_ID"))
                                                {
                                                    // -- ENROLL STUDENT
                                                    int student_id = session->get<int>("student_id");
                                                    Mapper<Enrolment> mapper(clientPtr);

                                                    //mapper const default in lamda capture
                                                    // rethink function object copies in async context

                                                    mapper.count( Criteria(Enrolment::Cols::_student_ID, CompareOperator::EQ, student_id),
                                                                 [mapper,student_id,group_id](unsigned long count)
                                                                {
                                                                     if(count < 1)
                                                                        {
                                                                         Enrolment enroll;
                                                                         enroll.setStudentId(student_id);
                                                                         enroll.setGroupId(group_id);

                                                                         mapper.insert(enroll,
                                                                                       []()
                                                                                       {
                                                                                          //redirect
                                                                                       },
                                                                                       dbexception
                                                                        );
                                                                        }
                                                                }
                                                                 ,dbexception);
                                                }
                                                else
                                                {
                                                    // new student - claim account
                                                }

                                            }
                                            else
                                            {
                                                //Auth failed
                                                auto resp = drogon::HttpResponse::newHttpResponse();
                                                resp->setStatusCode(drogon::k401Unauthorized);
                                                callback(resp);
                                            }
                                        }
                                    },
                                    dbexception,
                                    params.at("group_code"));




        /*
            //SELECT * FROM lessons INNER JOIN teachinggroups ON lessons.group_ID = teachinggroups.group_ID WHERE group_code = "10CS";

            // if join_code == EQUALS
            // use same transaction

            // if session
            // enrole session->student_ID in groupID

            // else claim student account - email
            // - get UUID - save to profile
            // - get session
            // enrole session->student_ID in groupID

        */
    }
    else
    {
        LOG_DEBUG<<"Malformed enroll by join_code attempt";
    }

}

/*
    Json::Value ret;

    if (params.size() > 0) {
        for (auto &[column, value] : params) {
            ret[column]=value;
        }
    }

    ret["token"]=drogon::utils::getUuid();
    auto resp=HttpResponse::newHttpJsonResponse(ret);
    callback(resp);
*/

/*
        Mapper<Teachinggroups> mapper(clientPtr);
        mapper.findOne( Criteria(Teachinggroups::Cols::_group_code, CompareOperator::EQ, params.at("group_code")) ,
                      [clientPtr,callback](Teachinggroups teaching_group)
                      {

                          teaching_group.getLessons(clientPtr);

                          Json::Value ret;
                          auto resp=HttpResponse::newHttpJsonResponse(ret);
                          callback(resp);
                      },

                      [](const drogon::orm::DrogonDbException &e)
                      {
                          std::cerr << "error:" << e.base().what() << std::endl;
                      }
                      );
*/
