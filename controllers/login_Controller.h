#pragma once

#include <drogon/HttpController.h>

using namespace drogon;

class LoginController : public drogon::HttpController<LoginController>
{
  public:
    METHOD_LIST_BEGIN
    ADD_METHOD_TO(LoginController::login,"/login",Post); //role email uuid
    ADD_METHOD_TO(LoginController::enroll,"/enroll",Post); //student_email group_code join_code
    //METHOD_ADD(LoginController::enroll,"/login?groupcode={1}email={2}&uuid={3}",Post);

    // use METHOD_ADD to add your custom processing function here;
    // METHOD_ADD(LoginController::get, "/{2}/{1}", Get); // path is /LoginController/{arg2}/{arg1}
    // METHOD_ADD(LoginController::your_method_name, "/{1}/{2}/list", Get); // path is /LoginController/{arg1}/{arg2}/list
    // ADD_METHOD_TO(LoginController::your_method_name, "/absolute/path/{1}/{2}/list", Get); // path is /absolute/path/{arg1}/{arg2}/list

    METHOD_LIST_END
    // your declaration of processing function maybe like this:
    // void get(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback, int p1, std::string p2);
    // void your_method_name(const HttpRequestPtr& req, std::function<void (const HttpResponsePtr &)> &&callback, double p1, int p2) const;

    void login(const HttpRequestPtr &req, std::function<void (const HttpResponsePtr &)> &&callback);
    /*
    void login(const HttpRequestPtr &req, std::function<void (const HttpResponsePtr &)> &&callback,
               std::string &&role,
               std::string &&email,
               const std::string &uuid);
    */

    void enroll(const HttpRequestPtr &req, std::function<void (const HttpResponsePtr &)> &&callback);

private:

};
