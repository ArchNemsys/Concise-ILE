# Work in progress !!! Not Yet Production Ready !!!
web assessment tool.

# License
Concise is avaliable under the terms of the [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) 

# Third Party Licenses
Concise graciously makes use of but is not affiliated with, nor authorised, endorsed or licensed in any way by the following :

## Server
* [Drogon - C++ web framework](https://drogon.org/) 

## Web
* [htmx](https://github.com/bigskysoftware/htmx) courtesy of Carson Gross et al used under the Zero-Clause BSD License

* [_hyperscript](https://github.com/bigskysoftware/_hyperscript) courtesy of Carson Gross et al used under the BSD 2-Clause License

* [Tailwind CSS](https://github.com/tailwindlabs/tailwindcss) courtesy of Adam Wathan used under the MIT License

* [daisyUI](https://github.com/saadeghi/daisyui) Tailwind CSS component library

## Client
* [Qt framework](https://www.qt.io/) courtesy of the Qt Company used under the conditions of the [LGPL Licence](https://www.gnu.org/licenses/lgpl-3.0.en.html).

* [Qt-AES](https://github.com/bricke/Qt-AES) courtesy of Matteo Brichese used under the conditions of the [UNLICENSE](http://unlicense.org/)

## Tools
* Qt Creator
* Vscodium

### IMSCC to Markdown
A thankyou to Shreya Shinde for their [Python utility to extract and parse IMSCC (IMS Common Cartridge) files](https://github.com/ShreyaShinde25/IMSCC-to-Markdown-Converter) 
It made migrating quizes to Concise much easier. 

## Special Thanks

### ULIB
The first version of Concise was based on the most excellent [ULib](https://github.com/stefanocasazza/ULib) by Stefano Casazza, to whom I am eternally grateful for all his help, patience and everything he taught me. 



